﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method_Overloading
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10;
            string b = a.ToString();

        }
    }

    //a class that adds two numbers of different types
    //demonstrates overloading of methods

    class Add_Two_Numbers
    {
        //notice how in each case I have used the same method name called 'add'
        //however, since the types are different, the complier knows that they are all
        //different methods, despite having the same

        public void add(int number_one,int number_two)
        {
            int result = number_one + number_two;
        }

        public void add(float number_one, float number_two)
        {
            float result = number_one + number_two;
        }

        public void add(int number_one, float number_two)
        {
            float result = number_one + number_two;
        }
    }
}
